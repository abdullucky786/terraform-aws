# Recipe app API Proxy

NGINX Proxy app for our recipe app api

## usage

### environmental Variables

* `LISTEN_PORT` - Port to listen on (default: `8080`)
* `APP-HOST` - hostname of the app to forward request to(default: `app`)
* `APP_PORT` - Port of the app  to forward request to (default: `9000`)
